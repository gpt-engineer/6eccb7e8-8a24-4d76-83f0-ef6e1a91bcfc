let timer;
let secondsLeft = 25 * 60;

function formatTime(seconds) {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;
}

function updateTimer() {
    document.getElementById('timer').textContent = formatTime(secondsLeft);
}

function startTimer() {
    clearInterval(timer);
    timer = setInterval(() => {
        secondsLeft--;
        updateTimer();
        if (secondsLeft <= 0) {
            clearInterval(timer);
        }
    }, 1000);
}

function pauseTimer() {
    clearInterval(timer);
}

function resetTimer() {
    clearInterval(timer);
    secondsLeft = 25 * 60;
    updateTimer();
}

function setSession() {
    clearInterval(timer);
    secondsLeft = 25 * 60;
    updateTimer();
}

function setShortBreak() {
    clearInterval(timer);
    secondsLeft = 5 * 60;
    updateTimer();
}

function setLongBreak() {
    clearInterval(timer);
    secondsLeft = 15 * 60;
    updateTimer();
}

document.getElementById('start').addEventListener('click', startTimer);
document.getElementById('pause').addEventListener('click', pauseTimer);
document.getElementById('reset').addEventListener('click', resetTimer);
document.getElementById('session').addEventListener('click', setSession);
document.getElementById('short-break').addEventListener('click', setShortBreak);
document.getElementById('long-break').addEventListener('click', setLongBreak);

updateTimer();
